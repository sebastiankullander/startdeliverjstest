const express = require("express")
const app = express()
const port = 3000

app.get("/api/v1/customer", (req, res) => {
    if (req.query.id) {
        return res.send([
            {
                id: req.query.id,
                name: "John Doe"
            }
        ])
    }
    return res.send([
        {
            id: 1,
            name: "John Doe"
        },
        {
            id: 2,
            name: "Jane Doe"
        },
        {
            id: 3,
            name: "Johnny Doe"
        }
    ])
})

app.post("/api/v1/customer", (req, res) => {
    return res.send({id: 1, name: "John Doe"})
})

app.put("/api/v1/customer/:id", (req, res) => {
    return res.send({id: 1, name: "John Doe"})
})

app.delete("/api/v1/customer/:id", (req, res) => {
    return res.send({})
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
