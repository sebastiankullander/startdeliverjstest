const axios = require('axios').default;

const customerEndpoint = "/api/v1/customer";

class APIClient {
    constructor(baseURL, apiKey) {
        this.instance = axios.create({
            baseURL: baseURL,
            headers: {"Authorization": apiKey}
        });
    }

    /**
     * @name getCustomerList
     * @description gets customer list from API
     * @param {int} id - id to retrieve specific customer
     * @param {string} name - retrieve customers with name
     * @param {int} limit - items per page
     * @param {int} offset - skip x items
     *
     * @returns {response} returns an axios response with customers
    */
    async getCustomerList(id, name, limit = 50, offset = 0) {
        try {
            const response = await this.instance.get(customerEndpoint, {
                params: {
                    id: id,
                    name: name,
                    limit: limit,
                    offset: offset
                }
            })
            return response;
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * @name createCustomer
     * @description creates a customer
     * @param {string} name - name to give the customer
     *
     * @returns {response} axios response with the created user
    */
    async createCustomer(name) {
        try {
            const response = await this.instance.post(customerEndpoint, {
                name: name
            })
            return response;
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * @name updateCustomer
     * @description updates a customer in the API
     * @param {int} id - specifies which customer to update
     * @param {string} name - updated name of the customer
     *
     * @returns {response} axios response with the updated customer
    */
    async updateCustomer(id, name) {
        try {
            const response = await this.instance.put(customerEndpoint + "/" + id, {
                name: name
            })
            return response;
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * @name deleteCustomer
     * @description deletes a customer from the API
     * @param {int} id - specifies which customer to delete
     *
     * @returns {response} empty axios response
    */
    async deleteCustomer(id) {
        try {
            const response = await this.instance.delete(customerEndpoint + "/" + id,);
            return response;
        } catch (error) {
            console.error(error);
        }
    }
}

module.exports = APIClient;
