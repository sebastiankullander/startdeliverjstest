const APIClient = require("./apiClient.js");

const primaryApiURL = "https://api1.example.com";
const secondaryApiURL = "https://api2.example.com";

const primaryApiKey = process.env.PRIMARYAPIKEY;
const secondaryApiKey = process.env.SECONDARYAPIKEY;

/**
  * @name customerSynchronization
  * @description Synchronizes primary API with secondary API
  *
  * @returns {null}
*/
async function customerSynchronization() {
    const primaryApiClient = new APIClient(primaryApiURL, primaryApiKey);
    const secondaryApiClient = new APIClient(secondaryApiURL, secondaryApiKey);

    let page = 0;
    while (true) {
        const primaryCustomerList = await primaryApiClient.getCustomerList(offset=page*50);

        await primaryCustomerList.data.forEach(async customer => {
            const secondaryCustomer = await secondaryApiClient.getCustomerList(customer.id);

            //if user exist update
            if (secondaryCustomer.status === 200) {
                //update customer
                if (customer.name != secondaryCustomer.data.name) {
                    secondaryApiClient.updateCustomer(customer.id, customer.name)
                }
            //if user does not exist create
        } else if (secondaryCustomer.status === 404) {
                secondaryApiClient.createCustomer(customer.name);
            }
        });
        page++;
        // because mockAPI does not support pagination so if we dont break like this it will be an
        // infinte loop
        if (page === 5) {
            break;
        }
    }
}

/**
* @name customerDelete
* @description Removes unexisting customers from secondary API
*
* @returns {null}
*/
async function customerDelete() {
    const primaryApiClient = new APIClient(primaryApiURL, primaryApiKey);
    const secondaryApiClient = new APIClient(secondaryApiURL, secondaryApiKey);

    let page = 0;
    while (true) {
        const secondaryCustomerList = await secondaryApiClient.getCustomerList(offset=page*50);

        await secondaryCustomerList.data.forEach(async customer => {
            const primaryCustomer = await primaryApiClient.getCustomerList(customer.id);

            if (primaryCustomer.status === 404) {
                secondaryApiClient.deleteCustomer(customer.id);
            }
        });
        page++;
        if (page === 5) {
            break;
        }
    }
}

customerSynchronization();
customerDelete();
