This program keeps two lists synchronized through their APIs.

To make sure that Axios responses were handled correctly, I threw together a mock api to work against.

How to run:
```bash
$ npm install
$ PRIMARYAPIKEY=example-api-key-1 SECONDARYAPIKEY=example-api-key-2 node index.js
```
